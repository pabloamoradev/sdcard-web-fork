#!/bin/bash

set -x

TMP_FOLDERS="internal/mounted-img internal/mount-temp"

umount internal/mounted-img
rm -rf $TMP_FOLDERS
rm -f public/generated-images/loading-*.gz
echo "--Clean Done--"
